﻿using GoodsProject.Models.Dto;

namespace GoodsProject.Repository
{
    public interface IGoodsRepository
    {
        Task<IEnumerable<GoodsDto>> GetGoods();
        Task<GoodsDto> GetGoodsById(int goodsId);
        Task DeleteGoods(int goodsId);
        Task AddGoods(GoodsDto dto);
    }
}
