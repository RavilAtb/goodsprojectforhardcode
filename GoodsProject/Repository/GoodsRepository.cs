﻿using AutoMapper;
using GoodsProject.DbContexts;
using GoodsProject.Models.Dto;
using GoodsProject.Models;
using Microsoft.EntityFrameworkCore;

namespace GoodsProject.Repository
{
    public class GoodsRepository : IGoodsRepository
    {
        readonly ApplicationDbContext _dbContext;
        private IMapper _mapper;

        public GoodsRepository(ApplicationDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<GoodsDto>> GetGoods()
        {
            var res = await _dbContext.Goods
                .Include(x => x.GoodsAttribute)
                .Include(x => x.Category).ToListAsync();

            return _mapper.Map<IEnumerable<GoodsDto>>(res);
        }

        public async Task<GoodsDto> GetGoodsById(int goodsId)
        {
            var res = await _dbContext.Goods
                .Include(c => c.Category)
                .Include(c => c.GoodsAttribute)
                .Where(x => x.Id == goodsId).FirstOrDefaultAsync();

            return _mapper.Map<GoodsDto>(res);
        }

        public async Task DeleteGoods(int goodsId)
        {
            var goods = await _dbContext.Goods.Where(x => x.Id == goodsId).FirstOrDefaultAsync();
            if (goods == null)
                throw new Exception("Товар для удаления не найден");
            _dbContext.Goods.Remove(goods);
            await _dbContext.SaveChangesAsync();
        }

        public async Task AddGoods(GoodsDto dto)
        {
            var goodsFull = await _dbContext.Goods.Where(x => x.GoodsName == dto.GoodsName).FirstOrDefaultAsync();
            if (goodsFull != null)
                throw new Exception("Данный товар уже существует");
            Goods goods = _mapper.Map<Goods>(dto);
            _dbContext.Goods.Add(goods);
            await _dbContext.SaveChangesAsync();
        }
    }
}
