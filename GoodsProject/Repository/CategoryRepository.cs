﻿using AutoMapper;
using GoodsProject.DbContexts;
using GoodsProject.Models.Dto;
using GoodsProject.Models;
using Microsoft.EntityFrameworkCore;

namespace GoodsProject.Repository
{
    public class CategoryRepository: ICategoryRepository
    {
        readonly ApplicationDbContext _dbContext;
        private IMapper _mapper;

        public CategoryRepository(ApplicationDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task AddCategory(CategoryDto dto)
        {
            var categoryFull = await _dbContext.Category.Where(c => c.CategoryName == dto.CategoryName).FirstOrDefaultAsync();
            if (categoryFull != null)
                throw new Exception("Данная категория уже существует");

            Category category = _mapper.Map<Category>(dto);
            if (category.Attributes == null)
            {
                category.Attributes = new List<CategoryAttribute>();
            }

            foreach (var attr in dto.DynamicAttributes)
            {
                var attribute = new CategoryAttribute
                {
                    Key = attr
                };
                category.Attributes.Add(attribute);
            }

            _dbContext.Category.Add(category);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteCategory(int categoryId)
        {
            var category = await _dbContext.Category.Where(c => c.Id == categoryId).FirstOrDefaultAsync();
            if (category == null)
                throw new Exception("Категория для удаления не найдена");
            _dbContext.Category.Remove(category);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Category>> GetCategoriyes()
        {
            return await _dbContext.Category
                .Include(x => x.Attributes)
                .ToListAsync();
        }
    }
}
