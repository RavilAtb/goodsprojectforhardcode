﻿using GoodsProject.Models.Dto;
using GoodsProject.Models;

namespace GoodsProject.Repository
{
    public interface ICategoryRepository
    {
        Task AddCategory(CategoryDto dto);
        Task DeleteCategory(int categoryId);
        Task<IEnumerable<Category>> GetCategoriyes();
    }
}
