﻿using GoodsProject.Models.Dto;
using GoodsProject.Repository;
using Microsoft.AspNetCore.Mvc;

namespace GoodsProject.Controllers
{
    [ApiController]
    [Route("api/category")]
    public class CategoryController : Controller
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryController(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        /// <summary>Add new category</summary>
        [HttpPost("addCategory")]
        public async Task<ActionResult> AddCategory(CategoryDto dto)
        {
            try
            {
                await _categoryRepository.AddCategory(dto);
                return NoContent();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Remove new category</summary>
        [HttpDelete("deleteCategory/{categoryId}")]
        public async Task<ActionResult> DeleteCategory(int categoryId)
        {
            try
            {
                await _categoryRepository.DeleteCategory(categoryId);
                return NoContent();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Get all categoriyes</summary>
        [HttpGet("getCategoriyes")]
        public async Task<ActionResult> GetCategoriyes()
        {
            try
            {
                return Ok(await _categoryRepository.GetCategoriyes());
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
