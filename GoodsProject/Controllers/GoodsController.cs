﻿using GoodsProject.Models.Dto;
using GoodsProject.Repository;
using Microsoft.AspNetCore.Mvc;

namespace GoodsProject.Controllers
{
    [ApiController]
    [Route("api/goods")]
    public class GoodsController : Controller
    {
        private readonly IGoodsRepository _goodsRepository;

        public GoodsController(IGoodsRepository goodsRepository)
        {
            _goodsRepository = goodsRepository;
        }

        /// <summary>Get all goods</summary>
        [HttpGet("getGoods")]
        public async Task<ActionResult> GetGoods()
        {
            try
            {
                return Ok(await _goodsRepository.GetGoods());
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        /// <summary>Get goods by id</summary>
        [HttpGet("getGoodsById/{goodsId}")]
        public async Task<ActionResult> GetGoodsById(int goodsId)
        {
            try
            {
                return Ok(await _goodsRepository.GetGoodsById(goodsId));
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Remove goods</summary>
        [HttpDelete("deleteGoods/{goodsId}")]
        public async Task<ActionResult> DeleteGoods(int goodsId)
        {
            try
            {
                await _goodsRepository.DeleteGoods(goodsId);
                return NoContent();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>Add new goods</summary>
        [HttpPost("addGoods")]
        public async Task<ActionResult> AddGoods(GoodsDto dto)
        {
            try
            {
                await _goodsRepository.AddGoods(dto);
                return NoContent();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
