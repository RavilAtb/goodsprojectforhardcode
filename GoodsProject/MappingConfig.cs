﻿using AutoMapper;
using GoodsProject.Models;
using GoodsProject.Models.Dto;

namespace GoodsProject
{
    public class MappingConfig
    {
        public static MapperConfiguration RegisterMaps()
        {
            var mappingConfig = new MapperConfiguration(config =>
            {
                config.CreateMap<GoodsDto, Goods>().ReverseMap();
                config.CreateMap<CategoryDto, Category>().ReverseMap();
                config.CreateMap<GoodsAttributeDto, GoodsAttribute>().ReverseMap();
            });

            return mappingConfig;
        }
    }
}
