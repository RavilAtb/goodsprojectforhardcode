﻿using GoodsProject.Models;
using Microsoft.EntityFrameworkCore;

namespace GoodsProject.DbContexts
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Category> Category { get; set; }
        public DbSet<Goods> Goods { get; set; }
        public DbSet<GoodsAttribute> GoodsAttribute { get; set; }
    }
}
