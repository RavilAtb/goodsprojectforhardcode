﻿using System.ComponentModel.DataAnnotations.Schema;

namespace GoodsProject.Models
{
    public class Goods
    {
        public int Id { get; set; }

        public string GoodsName { get; set; }

        public string GoodsFoto { get; set; }

        public string GoodsDescription { get; set; }

        public decimal GoodsPrice { get; set; }

        public int CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        public List<GoodsAttribute> GoodsAttribute { get; set; }
    }
}
