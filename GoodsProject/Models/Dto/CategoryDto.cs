﻿namespace GoodsProject.Models.Dto
{
    public class CategoryDto
    {
        public string CategoryName { get; set; }

        public List<string> DynamicAttributes { get; set; }
    }
}
