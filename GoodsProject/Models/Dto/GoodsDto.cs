﻿namespace GoodsProject.Models.Dto
{
    public class GoodsDto
    {
        public string GoodsName { get; set; }

        public string GoodsFoto { get; set; }

        public string GoodsDescription { get; set; }

        public decimal GoodsPrice { get; set; }

        public int CategoryId { get; set; }

        public List<GoodsAttributeDto> GoodsAttribute { get; set; }
    }
}
