﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace GoodsProject.Models
{
    public class CategoryAttribute
    {
        public int Id { get; set; }

        public int CategoryId { get; set; }

        public string Key { get; set; }

        [JsonIgnore]
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }
    }
}
