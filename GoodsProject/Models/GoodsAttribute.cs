﻿using System.ComponentModel.DataAnnotations.Schema;

namespace GoodsProject.Models
{
    public class GoodsAttribute
    {
        public int Id { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }

        public int GoodsId { get; set; }

        [ForeignKey("GoodsId")]
        public virtual Goods Goods { get; set; }
    }
}
