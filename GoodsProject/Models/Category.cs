﻿using System.ComponentModel;

namespace GoodsProject.Models
{
    public class Category
    {
        public int Id { get; set; }

        public string CategoryName { get; set; }

        public List<CategoryAttribute> Attributes { get; set; }
    }
}
